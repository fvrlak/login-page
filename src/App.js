import "./App.css";
import React, { useState, useMemo } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { ProvideAuth } from "./context/AuthContext";
import Login from "./components/Login";
import PrivateRoute from "./components/PrivateRoute";
import { UserNameContext } from "./context/UserNameContext";

function App() {
  const [userNameGlobal, setUserNameGlobal] = useState("");
  const providerValue = useMemo(() => ({ userNameGlobal, setUserNameGlobal }), [
    userNameGlobal,
    setUserNameGlobal,
  ]);

  return (
    <BrowserRouter>
      <ProvideAuth>
        <UserNameContext.Provider value={providerValue}>
          <Switch>
            <Route exact path="/" component={Login}></Route>
            <Route exact path="/home" component={PrivateRoute}></Route>
          </Switch>
        </UserNameContext.Provider>
      </ProvideAuth>
    </BrowserRouter>
  );
}

export default App;

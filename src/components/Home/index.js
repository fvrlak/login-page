import React, {useContext} from "react";
import {UserNameContext} from "../../context/UserNameContext"
export default function Home() {

  const context = useContext(UserNameContext)
  return (
    <div>{context && <div>{context.userNameGlobal}</div>}</div>
  )
 
}


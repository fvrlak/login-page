import React, { Component, useState, useEffect, useContext} from "react";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { Button, Grid, TextField, FormControl, Slide } from "@material-ui/core";
import { useCheckUserName } from "./useCheckUserName";
import { useHistory, Redirect } from "react-router-dom";
import { Alert, AlertTitle } from "@material-ui/lab";
import {UserNameContext} from "../../context/UserNameContext"
// // Requirements
// // use React function components
// // use Material-UI
// // use provided checkUsername function
// // use state handling (e.g. no email address as url argument)
// // checkUsername function
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  toolbar: {
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
  topMargin: {
    marginTop: theme.spacing.unit * 3,
  },
  container: {
    minHeight: "100vh",
  },
}));
function Login() {
  const history = useHistory();
  const classes = useStyles();
  const ctx = useContext(UserNameContext)
  
  const [count, setCount] = useState(0);

  const [error, setError] = useState("");
  const [errorCode, setErrorCode] = useState("");
  const [user, setUser] = useState("");
  const [errorFlag, setErrorFlag] = useState(false);
  const [disabled, setDisabled] = useState(true);

  const HandleTextFieldChange = (event) => {
    let incUserLength = user.length;
    incUserLength++;
    setDisabled(false)

    if (user.length === 0 && user.length === 17 && user.length === " ") {
      setDisabled(true)
      setErrorFlag(false);
    }
    //{error: null, data: "a@dig-it-ally.com"}
    //{error: "InvalidFormat", data: "Username can only consist of characters a-z, A-Z, '-', '_' and '.'"}
    useCheckUserName(event.target.value).then((res) => {
      setError(null);
      setErrorCode(null);
      setErrorFlag(false);

      if (res.error) {
        if (res.error === null) {
          setUser(res.data);
          setError(null);
          setErrorCode(null);
        }

        if (typeof res.error === "string") {
          setErrorCode(res.error);
          setErrorFlag(true);
          setDisabled(true)
        }
      }
      if (res.data) {
        const x = res.data.includes("@");
        x ? setUser(res.data) : setError(res.data);
      }
    });
  };

  const handleSubmit = (e) => {
    ctx.setUserNameGlobal(user)
    history.push("/home");
  };

  return (
    <div className={classes.content}>
      <Grid
        container
        spacing={16}
        direction="column"
        alignItems="center"
        justify="center"
        className={classes.container}
      >
        {error ?
        (
        <Alert severity="warning">
          <AlertTitle >{errorCode}</AlertTitle>
          {error}
        </Alert>
        //  </span>
        ) : null}
        <Grid item xs={12}>
          <TextField
            error={errorFlag}
            id="standard-basic"
            label="User"
            onChange={HandleTextFieldChange}
          />
        </Grid>
        <Grid item xs={12}></Grid>
        <Grid item xs={12}>
          <br></br>

          <Button
            variant="raised"
            color="primary"
            disabled={disabled}
            onClick={handleSubmit}
          >
            Login
          </Button>
        </Grid>
      </Grid>
    </div>
  );
}

export default Login;

// // Dig-it-ally hiring task
// // Create an app, that has a login-like screen containing 1 input field for username and a Login button. On clicking the Login button, call the provided checkUsername function. If the provided username is valid, checkUsername will return an email address. Handle returned errors. After successful login, show a new screen with the returned email address.

// // export async function checkUsername(username) {
// //    // simulate API response delay
// //    await new Promise((res) => setTimeout(res, 100));

// //    if (RegExp(/^[a-z-_.]+$/i).test(username) === false) {
// //        return { error: "InvalidFormat", data: "username can only consist of characters a-z, A-Z, '-', '_' and '.'" };
// //    }

// //    if (["admin", "null", "root"].includes(username)) {
// //        return { error: "AlreadyExists", data: "this username is already taken" };
// //    }

// //    return {
// //        error: null,
// //        data: `${username.toLowerif()}@dig-it-ally.com`,
// //    };
// // }
